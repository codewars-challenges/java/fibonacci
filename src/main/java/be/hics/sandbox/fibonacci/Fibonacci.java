package be.hics.sandbox.fibonacci;

import java.math.BigInteger;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class Fibonacci {

    private static Tuple<BigInteger> ZERO = new Tuple(BigInteger.ZERO, BigInteger.ZERO);
    private static Tuple<BigInteger> ONE = new Tuple(BigInteger.ONE, BigInteger.ONE);

    public static int fib(int n) {
        if (n < 0)
            throw new IllegalArgumentException("n should be positive!");
        UnaryOperator<Tuple<BigInteger>> op = t -> t.equals(ZERO) ? ONE : new Tuple(t._2, t._1.add(t._2));
        BigInteger result = Stream.iterate(ZERO, op).map(t -> t._1).limit(n + 1).reduce((first, second) -> second).orElse(null);
        return result.intValue();
    }

    public static class Tuple<X> {
        public final X _1;
        public final X _2;

        public Tuple(X _1, X _2) {
            this._1 = _1;
            this._2 = _2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Tuple<?> tuple = (Tuple<?>) o;
            if (!_1.equals(tuple._1)) return false;
            return _2.equals(tuple._2);
        }
    }

}