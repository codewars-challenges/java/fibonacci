package be.hics.sandbox.fibonacci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FibonacciApplication {

	public static void main(String[] args) {
		SpringApplication.run(FibonacciApplication.class, args);
		System.out.println(String.format("fib(%d) -> %d", 10, Fibonacci.fib(10)));
	}
}
